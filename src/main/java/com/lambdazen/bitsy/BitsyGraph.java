package com.lambdazen.bitsy;

import java.lang.management.ManagementFactory;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lambdazen.bitsy.store.FileBackedMemoryGraphStore;
import com.lambdazen.bitsy.store.MemoryGraphStore;
import com.lambdazen.bitsy.tx.BitsyTransaction;
import com.lambdazen.bitsy.tx.BitsyTransactionContext;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Element;
import com.tinkerpop.blueprints.Features;
import com.tinkerpop.blueprints.GraphQuery;
import com.tinkerpop.blueprints.KeyIndexableGraph;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.ThreadedTransactionalGraph;
import com.tinkerpop.blueprints.TransactionalGraph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.TransactionalGraph.Conclusion;
import com.tinkerpop.blueprints.util.DefaultGraphQuery;

public class BitsyGraph implements TransactionalGraph, KeyIndexableGraph, ThreadedTransactionalGraph, BitsyGraphMBean {
    private static final Logger log = LoggerFactory.getLogger(BitsyGraph.class);
    
    public static final double DEFAULT_REORG_FACTOR = 1; 
    public static final long DEFAULT_TX_LOG_THRESHOLD = 4 * 1024 * 1024;
    
    private boolean allowFullGraphScans;
    private boolean isPersistent;
    private Path dbPath;
    private ThreadLocal<ITransaction> curTransaction;
    private ThreadLocal<BitsyTransactionContext> curTransactionContext;
    private IGraphStore graphStore;
    private Features bitsyFeatures;
    private ObjectName objectName;
    private BitsyIsolationLevel defaultIsolationLevel;
    
    // Protected constructor used by ThreadedBitsyGraph
    protected BitsyGraph(char isThreaded, boolean allowFullGraphScans) {
        // char isThreaded is used to distinguish this constructor from others
        this.allowFullGraphScans = allowFullGraphScans;
    }
    
    public BitsyGraph() {
        this(true);
    }
    
    public BitsyGraph(boolean allowFullGraphScans) {
        this(null, allowFullGraphScans, -1, -1);
    }
    
    public BitsyGraph(Path dbPath) {
        this(dbPath, true, DEFAULT_TX_LOG_THRESHOLD, DEFAULT_REORG_FACTOR); // Default tx log size is 4MB
    }
    
    /** 
     * Constructor with all configurable parameters 
     * @param dbPath path to the database files
     * @param allowFullGraphScans whether/not iterations on vertices and edges should be supported
     * @param txLogThreshold the size of the transaction in bytes after which it will be scheduled to move to V/E files 
     * @param reorgFactor V/E reorgs are triggered when the size of the V/E files exceeds the initial size by (1 + factor) 
     */
    public BitsyGraph(Path dbPath, boolean allowFullGraphScans, long txLogThreshold, double reorgFactor) {
        this.dbPath = dbPath;
        this.allowFullGraphScans = allowFullGraphScans;
        this.curTransactionContext = new ThreadLocal<BitsyTransactionContext>();
        this.curTransaction = new ThreadLocal<ITransaction>();
        this.defaultIsolationLevel = BitsyIsolationLevel.REPEATABLE_READ;

        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        if (isPersistent()) {
            // Make sure that another BitsyGraph doesn't exist with the same path
            try {
                this.objectName = new ObjectName("com.lambdazen.bitsy", "path", ObjectName.quote(dbPath.toString()));
            } catch (MalformedObjectNameException e) {
                throw new BitsyException(BitsyErrorCodes.INTERNAL_ERROR, "Bug in quoting ObjectName", e);
            }
            
            // Check registry
            if (server.isRegistered(objectName)) {
                throw new BitsyException(BitsyErrorCodes.INSTANCE_ALREADY_EXISTS, "Path " + dbPath.toString());
            }
            
            // Load from files
            this.graphStore = new FileBackedMemoryGraphStore(new MemoryGraphStore(allowFullGraphScans), dbPath, txLogThreshold, reorgFactor);
        } else {
            this.graphStore = new MemoryGraphStore(allowFullGraphScans);
        }
        
        this.bitsyFeatures = new Features();

        bitsyFeatures.isPersistent = isPersistent; // Depends on setting

        // General
        bitsyFeatures.ignoresSuppliedIds = true;
        bitsyFeatures.isWrapper = false;
        bitsyFeatures.supportsDuplicateEdges = true;
        bitsyFeatures.supportsEdgeIndex = false;
        bitsyFeatures.supportsEdgeIteration = allowFullGraphScans;
        bitsyFeatures.supportsEdgeProperties = true;
        bitsyFeatures.supportsEdgeRetrieval = true;
        bitsyFeatures.supportsIndices = false;
        bitsyFeatures.supportsSelfLoops = true;
        bitsyFeatures.supportsThreadedTransactions = true;
        bitsyFeatures.supportsTransactions = true;
        bitsyFeatures.supportsVertexIndex = false;
        bitsyFeatures.supportsVertexIteration = allowFullGraphScans;
        bitsyFeatures.supportsVertexProperties = true;
        bitsyFeatures.supportsEdgeKeyIndex = true;
        bitsyFeatures.supportsKeyIndices = true;
        bitsyFeatures.supportsVertexKeyIndex = true;

        // Serialization
        bitsyFeatures.supportsBooleanProperty = true;
        bitsyFeatures.supportsDoubleProperty = true;
        bitsyFeatures.supportsDuplicateEdges = true;
        bitsyFeatures.supportsFloatProperty = true;
        bitsyFeatures.supportsIntegerProperty = true;
        bitsyFeatures.supportsLongProperty = true;
        bitsyFeatures.supportsMapProperty = true;
        bitsyFeatures.supportsMixedListProperty = true;
        bitsyFeatures.supportsPrimitiveArrayProperty = true;
        bitsyFeatures.supportsSerializableObjectProperty = true; 
        bitsyFeatures.supportsStringProperty = true;
        bitsyFeatures.supportsUniformListProperty = true;
        
        // Register this to the MBeanServer
        if (objectName != null) { 
            try {
                server.registerMBean(this, objectName);
            } catch (Exception e) {
                throw new BitsyException(BitsyErrorCodes.ERROR_REGISTERING_TO_MBEAN_SERVER, "Encountered exception", e);
            }
        }
    }
    
    public String toString() {
        if (dbPath != null) {
            return "bitsygraph[" + dbPath + "]";
        } else {
            return "bitsygraph[<in-memory>]";
        }
    }

    /** This method can be used to check if the current thread has an ongoing transaction */
    public boolean isTransactionActive() {
        ITransaction tx = curTransaction.get();
        
        return (tx != null);
    }

    public boolean isPersistent() {
        return (dbPath != null);
    }
    
    public boolean isFullGraphScanAllowed() {
        return allowFullGraphScans;
    }
    
    public BitsyIsolationLevel getDefaultIsolationLevel() {
    	return defaultIsolationLevel;
    }
    
    public void setDefaultIsolationLevel(BitsyIsolationLevel level) {
    	this.defaultIsolationLevel = level;
    }
    
    public BitsyIsolationLevel getTxIsolationLevel() {
    	return getTx().getIsolationLevel();
    }
    
    public void setTxIsolationLevel(BitsyIsolationLevel level) {
    	getTx().setIsolationLevel(level);	
    }
    
    public double getReorgFactor() {
        if (!isPersistent()) {
            throw new BitsyException(BitsyErrorCodes.OPERATION_UNDEFINED_FOR_NON_PERSISTENT_GRAPHS, "Reorg factor is only defined for persistent graphs (with a defined path to DB)");
        } else {
            return ((FileBackedMemoryGraphStore)graphStore).getVEReorgPotential().getFactor();
        }
    }
    
    public void setReorgFactor(double factor) {
        if (!isPersistent()) {
            throw new BitsyException(BitsyErrorCodes.OPERATION_UNDEFINED_FOR_NON_PERSISTENT_GRAPHS, "Reorg factor is only defined for persistent graphs (with a defined path to DB)");
        } else {
            ((FileBackedMemoryGraphStore)graphStore).getVEReorgPotential().setFactor(factor);
        }
    }
    
    public int getMinLinesPerReorg() {
        if (!isPersistent()) {
            throw new BitsyException(BitsyErrorCodes.OPERATION_UNDEFINED_FOR_NON_PERSISTENT_GRAPHS, "Reorg factor is only defined for persistent graphs (with a defined path to DB)");
        } else {
            return ((FileBackedMemoryGraphStore)graphStore).getVEReorgPotential().getMinLinesPerReorg();
        }
    }
    
    public void setMinLinesPerReorg(int minLinesPerReorg) {
        if (!isPersistent()) {
            throw new BitsyException(BitsyErrorCodes.OPERATION_UNDEFINED_FOR_NON_PERSISTENT_GRAPHS, "Reorg factor is only defined for persistent graphs (with a defined path to DB)");
        } else {
            ((FileBackedMemoryGraphStore)graphStore).getVEReorgPotential().setMinLinesPerReorg(minLinesPerReorg);
        }
    }
    
    public long getTxLogThreshold() {
        if (!isPersistent()) {
            throw new BitsyException(BitsyErrorCodes.OPERATION_UNDEFINED_FOR_NON_PERSISTENT_GRAPHS, "Transaction log threshold is only defined for persistent graphs (with a defined path to DB)");
        } else {
            return ((FileBackedMemoryGraphStore)graphStore).getTxLogFlushPotential().getTxLogThreshold();
        }
    }
    
    public void setTxLogThreshold(long txLogThreshold) {
        if (!isPersistent()) {
            throw new BitsyException(BitsyErrorCodes.OPERATION_UNDEFINED_FOR_NON_PERSISTENT_GRAPHS, "Transaction log threshold is only defined for persistent graphs (with a defined path to DB)");
        } else {
            ((FileBackedMemoryGraphStore)graphStore).getTxLogFlushPotential().setTxLogThreshold(txLogThreshold);
        }
    }

    /** This method flushes the transaction log to the V/E text files */
    public void flushTxLog() {
        if (!isPersistent()) {
            throw new BitsyException(BitsyErrorCodes.OPERATION_UNDEFINED_FOR_NON_PERSISTENT_GRAPHS, "Transaction log threshold is only defined for persistent graphs (with a defined path to DB)");
        } else {
            ((FileBackedMemoryGraphStore)graphStore).flushTxLog();
        }
    }

    /** This method backs up the database while it is still operational. Only one backup can be in progress at a time. 
     * 
     * @param pathToDir directory to which the database must be backed up.  
     */
    public void backup(String pathToDir) {
        backup(Paths.get(pathToDir));
    }
    
    /** This method backs up the database while it is still operational. Only one backup can be in progress at a time. 
     * 
     * @param pathToDir directory to which the database must be backed up.  
     */
    public void backup(Path pathToDir) {
        if (!isPersistent()) {
            throw new BitsyException(BitsyErrorCodes.OPERATION_UNDEFINED_FOR_NON_PERSISTENT_GRAPHS, "Transaction log threshold is only defined for persistent graphs (with a defined path to DB)");
        } else {
            ((FileBackedMemoryGraphStore)graphStore).backup(pathToDir);
        }
    }

    protected ITransaction getTx() {
        ITransaction tx = curTransaction.get();
        
        if (tx == null) {
            BitsyTransactionContext txContext = curTransactionContext.get();
            
            if (txContext == null) {
                txContext = new BitsyTransactionContext(graphStore);
                curTransactionContext.set(txContext);
            }
             
            tx = new BitsyTransaction(txContext, defaultIsolationLevel);
            
            curTransaction.set(tx);
        }
        
        return tx;
    }
    
    public Edge addEdge(Object id, Vertex outV, Vertex inV, String edgeLabel) {
        ITransaction tx = getTx();
        
        // Ignore the ID
        BitsyEdge edge = new BitsyEdge(UUID.randomUUID(), null, tx, BitsyState.M, 0, edgeLabel, (UUID)outV.getId(), (UUID)inV.getId());

        tx.addEdge(edge);
        
        return edge;
    }

    public Vertex addVertex(Object id) {
        ITransaction tx = getTx();
        
        // Ignore the ID
        BitsyVertex vertex = new BitsyVertex(UUID.randomUUID(), null, tx, BitsyState.M, 0);
        
        tx.addVertex(vertex);
        
        return vertex;
    }

    public Edge getEdge(Object id) {
        if (id == null) {
            throw new IllegalArgumentException("The edge ID passed to getEdge() is null");
        }
        
        if (id instanceof UUID) {
            return getTx().getEdge((UUID)id);
        } else if (id instanceof String) {
            // Get the UUID from the string representation -- may fail
            UUID uuid;
            try {
                uuid = UUID.fromString((String)id);
            } catch (IllegalArgumentException e) {
                // Decoding failed
                return null;
            }
            
            return getTx().getEdge(uuid);
        } else {
            // Wrong type
            return null;
        }
    }

    public Iterable<Vertex> getVertices() {
        if (!allowFullGraphScans) {
            throw new BitsyException(BitsyErrorCodes.FULL_GRAPH_SCANS_ARE_DISABLED, "Can not evaluate getVertices()");
        }
        
        final ITransaction tx = getTx();
        
        return new Iterable<Vertex>() {
            @Override
            public Iterator<Vertex> iterator() {
                return (Iterator)tx.getAllVertices();
            }
        };
    }

    public Iterable<Edge> getEdges() {
        final ITransaction tx = getTx();
        
        return new Iterable<Edge>() {
            @Override
            public Iterator<Edge> iterator() {
                return (Iterator)tx.getAllEdges();
            }
        };
    }

    public Features getFeatures() {
        return bitsyFeatures;
    }

    public Vertex getVertex(Object id) {
        if (id == null) {
            throw new IllegalArgumentException("The vertex ID passed to getVertex() is null");
        }
        
        Vertex ans;
        if (id instanceof UUID) {
            ans = getTx().getVertex((UUID)id);            
        } else if (id instanceof String) {
            // Get the UUID from the string representation -- may fail
            UUID uuid;
            try {
                uuid = UUID.fromString((String)id);
            } catch (IllegalArgumentException e) {
                // Decoding failed
                return null;
            }

            ans = getTx().getVertex(uuid);
        } else {
            // Wrong type
            ans = null;
        }
        
        return ans;
    }

    public void removeEdge(Edge e) {
        ITransaction tx = getTx();
        
        tx.removeEdge((BitsyEdge)e);
    }

    public void removeVertex(Vertex v) {
        ITransaction tx = getTx();

        tx.removeVertex((BitsyVertex)v);        
    }

    public void shutdown() {
        try {
            // As per Blueprints tests, shutdown() implies automatic commit
            commit();

            // Shutdown the underlying store
            graphStore.shutdown();
        } finally {        
            if (this.objectName != null) {
                // Deregister from JMX
                MBeanServer server = ManagementFactory.getPlatformMBeanServer();
                try {
                    server.unregisterMBean(objectName);
                } catch (Exception e) {
                    log.error("Error unregistering MBean named " + objectName + " from the MBeanServer", e);
                }
                objectName = null;
            }
        }
    }

    @Deprecated
    public void stopTransaction(Conclusion conc) {
        stopTx(conc == Conclusion.SUCCESS);
    }

    public <T extends Element> void createKeyIndex(String key, Class<T> elementType) {
        graphStore.createKeyIndex(key, elementType);
    }

    public <T extends Element> void dropKeyIndex(String key, Class<T> elementType) {
        graphStore.dropKeyIndex(key, elementType);
    }

    public <T extends Element> Set<String> getIndexedKeys(Class<T> elementType) {
        return graphStore.getIndexedKeys(elementType);
    }

    public Iterable<Vertex> getVertices(final String key, final Object value) {
        final ITransaction tx = getTx();
        
        return new Iterable<Vertex>() {
            @Override
            public Iterator<Vertex> iterator() {
                return (Iterator)tx.lookupVertices(key, value);
            }
        };
    }

    public Iterable<Edge> getEdges(final String key, final Object value) {
        final ITransaction tx = getTx();
        
        return new Iterable<Edge>() {
            @Override
            public Iterator<Edge> iterator() {
                return (Iterator)tx.lookupEdges(key, value);
            }
        };
    }
    
    public IGraphStore getStore() {
        return graphStore;
    }

    @Deprecated
    public TransactionalGraph startTransaction() {
        return newTransaction();
    }

    // New methods in 2.3.0
    @Override
    public GraphQuery query() {
        return new DefaultGraphQuery(this);
    }

    @Override
    public TransactionalGraph newTransaction() {
        return new ThreadedBitsyGraph(this);
    }

    @Override
    public <T extends Element> void createKeyIndex(String key, Class<T> elementClass, Parameter... indexParameters) {
        // Parameter wills will be ignored
        // TODO: Support unique indexes later
        createKeyIndex(key, elementClass);
    }

    @Override
    public void commit() {
        stopTx(true);
    }

    @Override
    public void rollback() {
        stopTx(false);
    }

    private void stopTx(boolean commitFlag) {
        ITransaction tx = curTransaction.get();

        if (tx == null) {
            // Nothing to do
        } else {
            try {
                tx.save(commitFlag); 
            } finally {
                // Remove it from the thread local -- in case of success or failure
                curTransaction.set(null);
            }
        }
    }
}
