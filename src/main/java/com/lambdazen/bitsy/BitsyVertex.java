package com.lambdazen.bitsy;

import java.util.ArrayList;
import java.util.Iterator;

import com.lambdazen.bitsy.ads.dict.Dictionary;
import com.lambdazen.bitsy.store.IStringCanonicalizer;
import com.lambdazen.bitsy.store.VertexBean;
import com.lambdazen.bitsy.store.VertexBeanJson;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.VertexQuery;
import com.tinkerpop.blueprints.util.DefaultVertexQuery;
import com.tinkerpop.blueprints.util.StringFactory;

public class BitsyVertex extends BitsyElement implements Vertex {
    private static final Direction[] directions = new Direction[] {Direction.OUT, Direction.IN}; 
            
    public BitsyVertex(UUID id, Dictionary properties, ITransaction tx, BitsyState state, int version) {
        super(id, properties, tx, state, version);
    }
    
    public BitsyVertex(VertexBean bean, ITransaction tx, BitsyState state) {
        this(bean.getId(), bean.getPropertiesDict(), tx, state, bean.getVersion());
    }

    public Iterable<Edge> getEdges(Direction dir, String... edgeLabels) {
        return tx.getEdges(this, dir, edgeLabels);
    }

    public VertexBean asBean() {
        // The TX is usually not active at this point. So no checks.
        return new VertexBean((UUID)id, properties, version);
    }

    public VertexBean asBean(IStringCanonicalizer canonicalizer) {
        if (properties != null) {
            properties.canonicalizeKeys(canonicalizer);
        }
        
        return asBean();
    }

    public VertexBeanJson asJsonBean() {
        // The TX is usually not active at this point. So no checks.
        //TreeMap<String, Object> propertyMap = (properties == null) ? null : properties.toMap();
        return new VertexBeanJson((UUID)id, properties, version, state);
    }

    public Iterable<Vertex> getVertices(final Direction dir, String... edgeLabels) {
        final ArrayList<Vertex> vertices = new ArrayList<Vertex>();
        
        for (Direction myDir : directions) {
            if ((myDir == dir) || (dir == Direction.BOTH)) {
                for (Edge e : getEdges(myDir, edgeLabels)) {
                    Vertex toAdd = e.getVertex(myDir.opposite());
                    vertices.add(toAdd);
                }
            }
        }
        
        // Go through the edges and load the vertices
        return new Iterable<Vertex>() {
            public Iterator<Vertex> iterator() {
                return vertices.iterator();
            }
        };
    }

    public VertexQuery query() {
        return new DefaultVertexQuery(this);
    }

    public void incrementVersion() {
        // It is OK for the version to wrap around MAX_INT
        this.version++;
    }

    public void remove() {
        tx.removeVertex(this);
    }

    @Override
    public Edge addEdge(String label, Vertex inVertex) {
        // Construct the edge with this as the out vertex 
        BitsyEdge edge = new BitsyEdge(UUID.randomUUID(), null, tx, BitsyState.M, 0, label, (UUID)getId(), (UUID)inVertex.getId());

        tx.addEdge(edge);
        
        return edge;
    }
    
    public String toString() {
        return StringFactory.vertexString(this);
    }
}
