package com.lambdazen.bitsy;

import java.util.Iterator;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

public interface ITransaction {
    public void save(boolean commit);
    
    public void validateForQuery(BitsyElement bitsyElement) throws BitsyException;

    public Vertex getVertex(UUID outVertexId) throws BitsyException;

    public Edge getEdge(UUID id) throws BitsyException;

    public Iterable<Edge> getEdges(BitsyVertex bitsyVertex, Direction dir, String... edgeLabels) throws BitsyException;
    
    public void markForPropertyUpdate(BitsyElement bitsyElement) throws BitsyException;

    public void addVertex(BitsyVertex vertex) throws BitsyException;

    public void removeVertex(BitsyVertex vertex) throws BitsyException;
    
    public void addEdge(BitsyEdge edge) throws BitsyException;

    public void removeEdge(BitsyEdge edge) throws BitsyException;

    public Iterator<BitsyVertex> getAllVertices();

    public Iterator<BitsyEdge> getAllEdges();

    public Iterator<BitsyVertex> lookupVertices(String key, Object value);
    
    public Iterator<BitsyEdge> lookupEdges(String key, Object value);

	public BitsyIsolationLevel getIsolationLevel();

	public void setIsolationLevel(BitsyIsolationLevel level);
}
